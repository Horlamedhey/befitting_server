const jsonwebtoken = require("jsonwebtoken");
require("dotenv").config();

module.exports = function(req, res, next) {
  if (
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    jsonwebtoken.verify(
      req.headers.authorization.split(" ")[1],
      process.env.secret,
      (err, decoded) => {
        if (err) {
          req.user = undefined;
        } else if (decoded.email === "admin@befittinglife.com") {
          req.user = decoded;
        }
        next();
      }
    );
  }
};
