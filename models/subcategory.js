const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./topic")
require("./course/course")
// const ObjectId = Schema.ObjectId
const SubCategorySchema = new Schema(
  {
    title: { type: String, index: true, unique: true },
    topics: [{ type: Schema.Types.ObjectId, ref: 'Topic' }],
    courses: [{ type: Schema.Types.ObjectId, ref: 'Course' }]
  }
);
const SubCategory = mongoose.model("SubCategory", SubCategorySchema);

module.exports = SubCategory;
