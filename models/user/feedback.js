const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./user")

// const ObjectId = Schema.ObjectId
const FeedbackSchema = new Schema(
  {
        name: { type: Schema.Types.ObjectId, ref: 'User' },
        feedback: { type: String, required: true },
        rating: { type: Number, required: true }
  },
  { timestamps: { createdAt: true, updatedAt: false } }
);
const Feedback = mongoose.model("Feedback", FeedbackSchema);

module.exports = Feedback;
