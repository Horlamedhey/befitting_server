const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;
// require("./plan")
require("./watched")
require('../course/course')
require('../course/video')

const UserSchema = new Schema(
  {
    name: { type: String, required: true, trim: true },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
    phone: { type: String, default: '' },
    isVerified: { type: Boolean, default: false },
    activationCode: String,
    nationality: { type: String, default: '' },
    // plan: { type: Schema.Types.ObjectId, ref: 'Plan' },
    courses: { type: Schema.Types.ObjectId, ref: 'Course' },
    watched: [{ type: Schema.Types.ObjectId, ref: 'Watched' }]
  },
  { timestamps: { createdAt: true, updatedAt: false } }
);
UserSchema.pre("save", function(next) {
  const user = this;
  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  });
});

UserSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};
const User = mongoose.model("User", UserSchema);

module.exports = User;
