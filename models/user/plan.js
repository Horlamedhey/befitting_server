const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const ObjectId = Schema.ObjectId
const PlanSchema = new Schema(
  {
    customer: String,
    subscription: String,
    subscriptionType: { type: String, default: "none" },
    subscriptions: [{id: String, package: String, price: Number, quantity: Number, total: Number, date: Date}],
    tokens: [{code: String, status: String, subType: String}],
    trial: { type: String, default: 'Unused' }
  }
);
const Plan = mongoose.model("Plan", PlanSchema);

module.exports = Plan;
