const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./user")
require("../course/video")

// const ObjectId = Schema.ObjectId
const WatchedSchema = new Schema(
  {
      title: String,
      user: { type: Schema.Types.ObjectId, ref: 'User' },
      videos: [{ type: Schema.Types.ObjectId, ref: 'Video' }]
  }
);
const Watched = mongoose.model("Watched", WatchedSchema);

module.exports = Watched;
