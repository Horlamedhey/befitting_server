const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("../maincategory")
require("../subcategory")
require("../topic")
// const ObjectId = Schema.ObjectId
const PostSchema = new Schema(
  {
      title: { type: String, unique: true, index: true, required: true, trim: true },
      slug: { type: String, trim: true },
      maincategory: { type: Schema.Types.ObjectId, ref: 'MainCategory', required: true },
      subcategory: { type: Schema.Types.ObjectId, ref: 'SubCategory', required: true },
      topic: { type: Schema.Types.ObjectId, ref: 'Topic', required: true },
      tags: [{ type: String, index: true }],
      content: { type: String, required: true },
      image: { type: String, required: true},
      views: { type: Number, default: 0 }
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);
const Post = mongoose.model("Post", PostSchema);

module.exports = Post;
