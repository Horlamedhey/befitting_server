const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./subcategory")
require("./course/course")
// const ObjectId = Schema.ObjectId
const MainCategorySchema = new Schema(
  {
        title: { type: String, index: true, unique: true },
        subcategories: [{ type: Schema.Types.ObjectId, ref: 'SubCategory' }],
        courses: [{ type: Schema.Types.ObjectId, ref: 'Course' }]
  }
);
const MainCategory = mongoose.model("MainCategory", MainCategorySchema);

module.exports = MainCategory;
