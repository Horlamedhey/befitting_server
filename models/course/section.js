const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./video")
// const ObjectId = Schema.ObjectId
const SectionSchema = new Schema(
  {
    title: { type: String, unique: true, required: true, trim: true },
    videos: [{ type: Schema.Types.ObjectId, ref: 'Video' }],
  }
);
const Section = mongoose.model("Section", SectionSchema);

module.exports = Section;
