const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const ObjectId = Schema.ObjectId
const VideoSchema = new Schema(
  {
    title: { type: String, unique: true, required: true },
    image: { type: String, required: true },
    link: { type: String, unique: true, required: true, trim: true }
  }
);
const Video = mongoose.model("Video", VideoSchema);

module.exports = Video;
