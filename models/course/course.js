const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("../maincategory")
require("../subcategory")
require("../topic")
require("./section")
// const ObjectId = Schema.ObjectId
const CourseSchema = new Schema(
  {
      title: { type: String, unique: true, index: true, required: true, trim: true },
      sections: [{ type: Schema.Types.ObjectId, ref: 'Section' }],
      slug: { type: String, trim: true },
      maincategory: { type: Schema.Types.ObjectId, ref: 'MainCategory', required: true },
      subcategory: { type: Schema.Types.ObjectId, ref: 'SubCategory', required: true },
      topic: { type: Schema.Types.ObjectId, ref: 'Topic', required: true },
      tags: [{ type: String, index: true }],
      content: { type: String, required: true },
      level: String,
      alt: String,
      status: String,
      price: String,
      upcoming: { type: Boolean, default: false},
      image: { type: String, required: true},
      buys: { type: Number, default: 0 }
  },
  { timestamps: { createdAt: true, updatedAt: true } }
);
const Course = mongoose.model("Course", CourseSchema);

module.exports = Course;
