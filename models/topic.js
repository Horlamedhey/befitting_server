const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("./course/course")
// const ObjectId = Schema.ObjectId
const TopicSchema = new Schema(
  {
    title: { type: String, index: true, unique: true },
    courses: [{ type: Schema.Types.ObjectId, ref: 'Course' }]
  }
);
const Topic = mongoose.model("Topic", TopicSchema);

module.exports = Topic;
