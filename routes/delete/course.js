const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Course = require("../../models/course/course");

router.patch(
  "/removeCourse",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Course.updateOne(
      { $pull: { courses: { _id: req.body.id } } },
      (err, newCourse) => {
        if (err) {
          res.sendStatus(500);
        }
        res.sendStatus(200);
      }
    );
  }
);

module.exports = router;
  