const { Router } = require("express");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const MainCategory = require("../../models/maincategory");
const Post = require("../../models/blog/post");
require("dotenv").config();
const stripe = require('stripe')(process.env.stripeSecret);
cloudinary.config({
  cloud_name: process.env.cloudinaryName,
  api_key: process.env.cloudinaryKey,
  api_secret: process.env.cloudinarySecret
});
const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "blog",
  filename: function(req, file, cb) {
    cb(null, file.originalname.split(".")[0]);
  }
});
const parser = multer({ storage: storage, fileSize: 204800 });
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");

router.patch(
  "/removeMainCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.updateOne(
      { $pull: { categories: { _id: req.body.id } } },
      (err, newCategory) => {
        if (err) {
          res.sendStatus(500);
        }
        res.sendStatus(200);
      }
    );
  }
);

router.patch(
  "/removeSubCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.updateOne(
      { $pull: { moduleCategories: { _id: req.body.id } } },
      (err, newCategory) => {
        if (err) {
          res.sendStatus(500);
        }
        res.sendStatus(200);
      }
    );
  }
);

router.patch(
  "/removeTopic",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.updateOne(
      { $pull: { skills: { _id: req.body.id } } },
      (err, newSkill) => {
        if (err) {
          res.sendStatus(500);
        }
        res.sendStatus(200);
      }
    );
  }
);

module.exports = router;

// router.patch(
//   "/removePackages",
//   (req, res, next) => {
//     setAdmin(req, res, next);
//   },
//   async (req, res) => {
//     await stripe.coupons.del(req.body.id.toLowerCase().split(' ').join('-'));
//     await stripe.plans.del(req.body.id);
//     const deletion = await stripe.plans.del(req.body.id + '-trial');
//     res.send(deletion);
//   }
// );
