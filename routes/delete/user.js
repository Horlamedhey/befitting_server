const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Feedback = require("../../models/user/feedback");

router.patch(
  "/removeFeedback",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Feedback.updateOne(
      { $pull: { feedbacks: { _id: req.body.id } } },
      (err, newPackage) => {
        if (err) {
          res.sendStatus(500);
        } else {
          res.sendStatus(200);
        }
      }
    );
  }
);

module.exports = router;
  