const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Course = require("../../models/course/course");

router.patch(
  "/saveCourse",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    const query = {};
    query["courses." + req.body.index] = req.body.course;
    Admin.updateOne({
      $set: query
    }, (err, newCourse) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  }
);

router.patch("/incrementCourseViews", (req, res) => {
  const query = {};
  query["courses." + req.body.index + ".views"] = 1;
  Admin.updateOne({
    $inc: query
  }, (err, admin) => {
    if (err) {
      res.sendStatus(500);
    } else {
      res.sendStatus(200);
    }
  });
});

module.exports = router;
  