const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Post = require("../../models/blog/post");

router.patch(
  "/savePost",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    const query = {};
    query["blog." + req.body.index] = req.body.post;
    Admin.updateOne({
      $set: query
    }, (err, newPost) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  }
);

router.patch("/incrementPostReads", (req, res) => {
  const query = {};
  query["blog." + req.body.index + ".views"] = 1;
  Admin.updateOne({
    $inc: query
  }, (err, admin) => {
    if (err) {
      res.sendStatus(500);
    } else {
      res.sendStatus(200);
    }
  });
});

module.exports = router;
  