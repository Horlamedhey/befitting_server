const { Router } = require("express");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const MainCategory = require("../../models/maincategory");
const Post = require("../../models/blog/post");
require("dotenv").config();
const stripe = require('stripe')(process.env.stripeSecret);
cloudinary.config({
  cloud_name: process.env.cloudinaryName,
  api_key: process.env.cloudinaryKey,
  api_secret: process.env.cloudinarySecret
});
const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "blog",
  filename: function(req, file, cb) {
    cb(null, file.originalname.split(".")[0]);
  }
});
const parser = multer({ storage: storage, fileSize: 204800 });
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");


router.patch(
  "/saveMainCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    const query = {};
    query["categories." + req.body.index] = req.body.category;
    Admin.updateOne({
      $set: query
    }, (err, newCategory) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  }
);

router.patch(
  "/saveSubCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    const query = {};
    query["moduleCategories." + req.body.index] = req.body.category;
    Admin.updateOne({
      $set: query
    }, (err, newCategory) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  }
);

router.patch(
  "/saveTopic",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    const query = {};
    query["skills." + req.body.index] = req.body.skill;
    Admin.updateOne({
      $set: query
    }, (err, newSkill) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });
  }
);


module.exports = router;
