const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const User = require("../../models/user/user");

router.post("/verifyMail", (req, res) => {
  User.findOne({
    activationCode: req.body.code
  }, (err, user) => {
    if (err) {
      res.status(406).send("Activation failed! Please try again.");
    } else if (user) {
      const time = new Date().getHours() - new Date(user.updatedAt).getHours();
      if (time >= 2) {
        res.send("Link expired!");
      } else {
        User.findOneAndUpdate({
            activationCode: req.body.code
          }, {
            isVerified: true,
            $unset: {
              activationCode: 1
            }
          }, {
            new: true
          },
          (err, user) => {
            if (err) {
              res.status(406).send("Activation failed! Please try again.");
            } else if (user) {
              res.send("Success");
            }
          }
        );
      }
    } else {
      res.sendStatus(200);
    }
  });
});

router.put("/setWatched",
(req, res, next) => {
  setUser(req, res, next);
}, (req, res) => {
  const query = {};
  query["watched." + req.body.index] = {
    title: req.body.title,
    videos: req.body.videos
  };
  User.findOneAndUpdate({
      email: req.body.email
    }, {
      $set: query
    }, {
      new: true
    },
    (err, result) => {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    }
  );
});

router.patch(
"/updateUser",
(req, res, next) => {
  setUser(req, res, next);
},
(req, res) => {
  User.findByIdAndUpdate(
    req.body.id, {
      nationality: req.body.nationality,
      phone: req.body.phone
    }, {
      new: true
    },
    (err, newUser) => {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    }
  );
}
);

router.post('/buyTokens',
(req, res, next) => {
  setUser(req, res, next);
}, async (req, res) => {
  const {
    tokens,
    uid,
    ...others
  } = req.body;
  const charge = await stripe.charges.create(others, function (err, charge) {
    if (err) {
      console.log(err)
      res.sendStatus(500);
    } else {
      console.log(tokens[0]);
      const payment = {
        id: charge.balance_transaction,
        package: 'Tokens',
        price: charge.amount / tokens.length,
        quantity: tokens.length,
        total: charge.amount,
        date: new Date(charge.created * 1000)
      };
      User.findByIdAndUpdate(
        uid, {
          $addToSet: {
            "plan.tokens": tokens
          },
          $push: {
            "plan.subscriptions": payment
          },
        }, {
          new: true
        },
        (err, newUser) => {
          if (err) {
            res.sendStatus(500);
          } else {
            res.sendStatus(200);
          }
        }
      );
      //     res.send(charge);
    }
  });
})

router.post('/redeemToken',
(req, res, next) => {
  setUser(req, res, next);
}, async (req, res) => {
  const body = req.body;
  User.findById(body.tokenOwner, async (err, tokenOwner) => {
    const token = tokenOwner.plan.tokens.filter(v => {
      return v.code === body.token;
    })[0]
    if (token) {
      if (token.status === 'Used') {
        res.status(400).send('Token already used!');
      } else {
        User.findById(body.uid, async (err, user) => {
          if (err) {
            res.sendStatus(400)
          } else {
            await stripe.customers.del(user.plan.customer);
          }
        });
        const customer = await stripe.customers.create({
          email: body.email
        });
        await stripe.subscriptions.create({
          customer: customer.id,
          items: [{
            plan: token.subType.toLowerCase().split(' ').join('-')
          }],
          cancel_at_period_end: true,
          coupon: token.subType.toLowerCase().split(' ').join('-'),
        });
        const tokens = await tokenOwner.plan.tokens;
        tokens.forEach(v => {
          if (v.code === body.token) {
            v.status = 'Used';
          }
        });
        User.findByIdAndUpdate(body.tokenOwner, {
          "plan.tokens": tokens
        }, async (err, user) => {
          if (err) {
            res.sendStatus(400)
          } else {
            res.sendStatus(200);
          }
        })
      }
    } else {
      res.status(400).send('Token verification failed!');
    }
  })
})

router.post('/deleteToken',
(req, res, next) => {
  setUser(req, res, next);
}, async (req, res) => {
  const {
    tokens,
    uid
  } = req.body;
  User.findByIdAndUpdate(
    uid, {
      "plan.tokens": tokens
    }, {
      new: true
    },
    (err, newUser) => {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    }
  );
})

router.post('/subscribe', (req, res) => {
const data = req.body.data.object;
const email = data.customer_email;
const customer = data.customer;
const subscription = data.lines.data[0].subscription;
const nickname = data.lines.data[0].plan.nickname;
if (data.amount_due === 0 && data.discount === null) {
  User.findOneAndUpdate({
      email: email
    }, {
      "plan.customer": customer,
      "plan.subscription": subscription,
      "plan.subscriptionType": nickname,
      "plan.trial": 'Used'
    }, {
      new: true
    },
    (err, newUser) => {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    }
  );
} else if (data.amount_due === 0 && data.discount !== null) {
  User.findOneAndUpdate({
        email: email
      }, {
        "plan.customer": customer,
        "plan.subscription": subscription,
        "plan.subscriptionType": nickname,
      }, {
        new: true
      },
      (err, newUser) => {
        if (err) {
          res.sendStatus(500);
        } else {
          res.sendStatus(200);
        }
      }
  );
} else {
  const subscriptions = {
    id: data.number,
    package: nickname,
    price: data.amount_paid,
    quantity: data.lines.data[0].quantity,
    total: data.total,
    date: new Date(data.status_transitions.paid_at * 1000)
  };
  if (subscriptions.quantity > 1) {
    console.log('res.sendStatus(200)')
  } else {
    User.findOneAndUpdate({
        email: email
      }, {
        "plan.customer": customer,
        "plan.subscription": subscription,
        "plan.subscriptionType": nickname,
        $push: {
          "plan.subscriptions": subscriptions
        },
      }, {
        new: true
      },
      (err, newUser) => {
        if (err) {
          res.sendStatus(500);
        } else {
          res.sendStatus(200);
        }
      }
    );
  }
}
});

router.post('/cancelSub', (req, res) => {
User.findOneAndUpdate({
    "plan.subscription": req.body.data.object.id
  }, {
    "plan.subscriptionType": 'none'
  }, {
    new: true
  },
  async (err, newUser) => {
    if (err) {
      res.sendStatus(500);
    } else {
      await stripe.customers.del(newUser.plan.customer);
      res.sendStatus(200);
    }
  }
);
})
router.post('/cancelSubscription',
(req, res, next) => {
  setUser(req, res, next);
}, async (req, res) => {
  const subscription = await stripe.subscriptions.update(req.body.id, {
    cancel_at_period_end: true
  });
  if (subscription.cancel_at_period_end) {
    res.sendStatus(200);
  } else {
    res.sendStatus(500);
  };
});

router.patch(
  "/modMembership",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    User.findByIdAndUpdate(
      req.body.id, {
        "plan.subscriptionType": req.body.action === "upgrade" ?
          "Premium" : req.body.action === "downgrade" ?
          "none" : null
      },
      (err, result) => {
        if (err) {
          res.sendStatus(500);
        } else if (result) {
          res.sendStatus(200);
        }
      }
    );
  }
);

module.exports = router;
