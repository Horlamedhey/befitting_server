const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Course = require("../../models/course/course");


router.get("/getCourseCount", (req, res) => {
  const category = req.query.category;
  if (category === "All") {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $size: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: { $eq: [false, "$$item.upcoming"] }
                  }
                }
              ]
            }
          }
        }
      ],
      (err, result) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(result[0]);
      }
    );
  } else if (
    category === "Beginner" ||
    category === "Intermediate" ||
    category === "Advanced"
  ) {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $size: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: {
                      $and: [
                        { $eq: [false, "$$item.upcoming"] },
                        { $eq: [category, "$$item.level"] }
                      ]
                    }
                  }
                }
              ]
            }
          }
        }
      ],
      (err, result) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(result[0]);
      }
    );
  } else {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $size: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: {
                      $and: [
                        { $eq: [false, "$$item.upcoming"] },
                        { $in: [category, "$$item.tags"] }
                      ]
                    }
                  }
                }
              ]
            }
          }
        }
      ],
      (err, result) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(result[0]);
      }
    );
  }
});

router.get("/fetchCourses", (req, res) => {
  const category = req.query.category;
  const skip = parseInt(req.query.skip);
  if (category === "All") {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $slice: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: { $gt: [{ $size: ["$$item.modules"] }, 0] }
                  }
                },
                skip,
                10
              ]
            }
          }
        },
        {
          $unwind: "$courses"
        },
        {
          $project: {
            course: "$courses",
            _id: 0
          }
        }
      ],
      (err, admin) => {
        if (err) {
          console.log(err);
        } else {
          const courses = [];
          admin.forEach(v => {
            courses.push(v.course);
          });
          res.send(courses);
        }
      }
    );
  } else if (
    category === "Beginner" ||
    category === "Intermediate" ||
    category === "Advanced"
  ) {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $slice: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: {
                      $and: [
                        { $eq: [category, "$$item.level"] },
                        { $gt: [{ $size: ["$$item.modules"] }, 0] }
                      ]
                    }
                  }
                },
                skip,
                10
              ]
            }
          }
        },
        {
          $unwind: "$courses"
        },
        {
          $project: {
            course: "$courses",
            _id: 0
          }
        }
      ],
      (err, admin) => {
        if (err) {
          console.log(err);
        } else {
          const courses = [];
          admin.forEach(v => {
            courses.push(v.course);
          });
          res.send(courses);
        }
      }
    );
  } else if (category === undefined) {
    Admin.findOne({}, (err, admin) => {
      if (err) {
        console.log(err);
      } else {
        res.send(admin.courses);
      }
    });
  } else {
    Admin.aggregate(
      [
        {
          $project: {
            courses: {
              $slice: [
                {
                  $filter: {
                    input: "$courses",
                    as: "item",
                    cond: {
                      $and: [
                        { $in: [category, "$$item.tags"] },
                        { $gt: [{ $size: ["$$item.modules"] }, 0] }
                      ]
                    }
                  }
                },
                skip,
                10
              ]
            }
          }
        },
        {
          $unwind: "$courses"
        },
        {
          $project: {
            course: "$courses",
            _id: 0
          }
        }
      ],
      (err, admin) => {
        if (err) {
          console.log(err);
        } else {
          const courses = [];
          admin.forEach(v => {
            courses.push(v.course);
          });
          res.send(courses);
        }
      }
    );
  }
});

router.get("/fetchCourse/:slug", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      const course = admin.courses.find(v => {
        return v.slug === req.params.slug;
      });
      res.send({ course: course, identity: admin.courses.indexOf(course) });
    }
  });
});

router.get("/fetchPopularCourses", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualCourses = [];
      const courses = admin.courses.sort((a, b) => {
        return b.views - a.views;
      });
      if (courses.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualCourses.push(courses[i]);
        }
      } else {
        actualCourses = courses;
      }
      res.send(actualCourses);
    }
  });
});

router.get("/fetchPopularCourses/:slug", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualCourses = [];
      let courses = admin.courses.sort((a, b) => {
        return b.views - a.views;
      });
      courses = courses.filter(v => {
        return v.slug !== req.params.slug && !v.upcoming;
      });
      if (courses.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualCourses.push(courses[i]);
        }
      } else {
        actualCourses = courses;
      }
      res.send(actualCourses);
    }
  });
});

router.get("/fetchUpcomingCourses", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualCourses = [];
      const courses = admin.courses.filter(v => {
        return v.upcoming;
      });
      if (courses.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualCourses.push(courses[i]);
        }
      } else {
        actualCourses = courses;
      }
      res.send(actualCourses);
    }
  });
});

router.get("/fetchRelatedCourses/:slug", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualCourses = [];
      const theCourses = [];
      const courses = admin.courses.filter(v => {
        return (
          v.slug !== req.params.slug && !v.upcoming && v.modules.length > 0
        );
      });
      const course = admin.courses.find(v => {
        return v.slug === req.params.slug;
      });
      course.tags.forEach(u => {
        courses.forEach(v => {
          if (v.tags.includes(u)) {
            theCourses.push(v);
          }
        });
      });
      if (theCourses.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualCourses.push(theCourses[i]);
        }
      } else {
        actualCourses = theCourses;
      }
      res.send(actualCourses);
    }
  });
});

module.exports = router;
  