const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const User = require("../../models/user/user");

router.post("/login", (req, res) => {
  User.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      res.status(500).send({
        text: "Internal Server Error! Please try again.",
        color: "error",
        icon: "mdi-alert-decagram-outline",
        time: 6000
      });
    } else if (!user) {
      res.status(404).send({
        text: "Login failed! User not found.",
        color: "error",
        icon: "mdi-alert-decagram-outline",
        time: 6000
      });
    } else if (user) {
      if (!user.comparePassword(req.body.password)) {
        res.status(404).send({
          text: "Login failed! Incorrect Password.",
          color: "error",
          icon: "mdi-alert-decagram-outline",
          time: 6000
        });
      } else if (!user.isVerified) {
        res.status(404).send({
          text: "Login failed!",
          color: "warning",
          icon: "mdi-alert-decagram-outline",
          time: 0,
          addition: "Please check your mailbox to activate your account.",
          addAction: true,
          actiontype: "resend",
          email: user.email
        });
      } else {
        const {
          plan,
          isVerified,
          _id,
          name,
          email,
          mails,
          nationality,
          phone
        } = user;
        const token = jsonwebtoken.sign({
            plan,
            isVerified,
            _id,
            name,
            email,
            mails,
            nationality,
            phone
          },
          process.env.secret, {
            expiresIn: "6h"
          }
        );
        return res.send({
          accessToken: token,
          user: {
            plan,
            isVerified,
            _id,
            name,
            email,
            mails,
            nationality,
            phone
          }
        });
      }
    }
  });
});

router.get(
  "/user",
  (req, res, next) => {
    setUser(req, res, next);
  },
  (req, res) => {
    if (req.refetch) {
      User.findOne({
        email: req.user.email
      }, function (err, user) {
        if (err) {
          res.sendStatus(500);
        } else {
          const {
            plan,
            isVerified,
            _id,
            name,
            email,
            mails,
            nationality,
            phone
          } = user;
          const token = jsonwebtoken.sign({
                plan,
                isVerified,
                _id,
                name,
                email,
                mails,
                nationality,
                phone
              },
              process.env.secret, {
                expiresIn: "6h"
              }
          );
          return res.send({
            accessToken: token,
            user: {
              plan,
              isVerified,
              _id,
              name,
              email,
              mails,
              nationality,
              phone
            }
          });
        }
      })
    }else {
  const {
    plan,
    isVerified,
    _id,
    name,
    email,
    mails,
    nationality,
    phone
  } = req.user;
  res.send({
    plan,
    isVerified,
    _id,
    name,
    email,
    mails,
    nationality,
    phone
  });
  }
  }
);

router.get("/fetchFeedbacks", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      res.send(admin.feedbacks);
    }
  });
});

router.get(
  "/fetchUsers",
  (req, res, next) => setAdmin(req, res, next),
  (req, res) => {
    User.findById(req.user._id, (err, user) => {
      if (err) {
        console.log(err);
      } else if (user.email === req.user.email && user.name === req.user.name) {
        User.find(
          { email: { $ne: user.email } },
          "_id name email nationality plan createdAt",
          (err, users) => {
            if (err) {
              console.log(err);
            } else {
              res.send(users);
            }
          }
        );
      }
    });
  }
);

router.post("/getWatched", (req, res) => {
  User.findOne(
    { email: req.body.email },
    "watched",
    { lean: true },
    (err, result) => {
      if (err) {
        res.sendStatus(500);
      } else if (result.watched === undefined) {
        res.send({ video: [], index: 0 });
      } else {
        const watched = Object.values(result.watched);
        const video = watched.find(v => {
          return v.title === req.body.title;
        });
        if (video === undefined) {
          res.send({ video: [], index: watched.length });
        } else {
          res.send({
            video: watched[watched.indexOf(video)].videos,
            index: watched.indexOf(video)
          });
        }
      }
    }
  );
});

module.exports = router;
  