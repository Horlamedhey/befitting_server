const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Post = require("../../models/blog/post");

router.get("/getBlogCount", (req, res) => {
  Admin.aggregate(
    [{ $project: { blog: { $size: "$blog" } } }],
    (err, result) => {
      if (err) {
        res.sendStatus(500);
      }
      res.send(result[0]);
    }
  );
});

router.get("/fetchBlog", (req, res) => {
  // const skip = parseInt(req.query.skip);
  Post.find({})
    .then(blog => {
      res.send(blog);
    }).catch(err => {
    console.log(err);
    });
});

router.get("/fetchPost/:slug", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      const post = admin.blog.find(v => {
        return v.slug === req.params.slug;
      });
      res.send({ post: post, identity: admin.blog.indexOf(post) });
    }
  });
});

router.get("/fetchPopularPosts", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualPosts = [];
      const posts = admin.blog.sort((a, b) => {
        return b.views - a.views;
      });
      if (posts.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualPosts.push(posts[i]);
        }
      } else {
        actualPosts = posts;
      }
      res.send(actualPosts);
    }
  });
});

router.get("/fetchPopularPosts/:slug", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      let actualPosts = [];
      let posts = admin.blog.sort((a, b) => {
        return b.views - a.views;
      });
      posts = posts.filter(v => {
        return v.slug !== req.params.slug && !v.upcoming;
      });
      if (posts.length >= 4) {
        for (let i = 0; i < 4; i++) {
          actualPosts.push(posts[i]);
        }
      } else {
        actualPosts = posts;
      }
      res.send(actualPosts);
    }
  });
});

module.exports = router;
  