const { Router } = require("express");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const MainCategory = require("../../models/maincategory");
const Post = require("../../models/blog/post");
require("dotenv").config();
const stripe = require('stripe')(process.env.stripeSecret);
cloudinary.config({
  cloud_name: process.env.cloudinaryName,
  api_key: process.env.cloudinaryKey,
  api_secret: process.env.cloudinarySecret
});
const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "blog",
  filename: function(req, file, cb) {
    cb(null, file.originalname.split(".")[0]);
  }
});
const parser = multer({ storage: storage, fileSize: 204800 });
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");


router.get("/fetchMainCategories", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      res.send(admin.categories);
    }
  });
});

router.get("/fetchSubCategories", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      res.send(admin.moduleCategories);
    }
  });
});

router.get("/fetchTopicss", (req, res) => {
  Admin.findOne({}, (err, admin) => {
    if (err) {
      console.log(err);
    } else {
      res.send(admin.moduleCategories);
    }
  });
});


module.exports = router;

// router.get(
//   "/getPackages",
//   async (req, res) => {
//     const plans = await stripe.plans.list();
//     res.send(plans);
//   }
// );
