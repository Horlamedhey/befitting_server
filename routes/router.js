const { Router } = require("express");
const router = Router();

//  Create Endpoints

const createBlog = require("./create/blog");
const createCourse = require("./create/course");
const createGeneral = require("./create/general");
const createUser = require("./create/general");

router.use(createBlog);
router.use(createCourse);
router.use(createGeneral);
router.use(createUser);

//  Read Endpoints

const readBlog = require("./read/blog");
const readCourse = require("./read/course");
const readGeneral = require("./read/general");
const readUser = require("./read/user");

router.use(readBlog);
router.use(readCourse);
router.use(readGeneral);
router.use(readUser);

//  Update Endpoints

const updateBlog = require("./update/blog");
const updateCourse = require("./update/course");
const updateGeneral = require("./update/general");
const updateUser = require("./update/user");

router.use(updateBlog);
router.use(updateCourse);
router.use(updateGeneral);
router.use(updateUser);

//  Delete Endpoints

const deleteBlog = require("./delete/blog");
const deleteCourse = require("./delete/course");
const deleteGeneral = require("./delete/general");
const deleteUser = require("./delete/user");

router.use(deleteBlog);
router.use(deleteCourse);
router.use(deleteGeneral);
router.use(deleteUser);

module.exports = router;