const { Router } = require("express");
const router = Router();
const setUser = require("../../middlewares/setUser");
const Post = require("../models/blog/post");
const User = require("../../models/user/user");
const Feedback = require("../../models/user/feedback");

router.post("/register", (req, res) => {
    const credentials = req.body;
    User.create(credentials, function (err, user) {
      if (err) {
        if (/duplicate/.test(err.message)) {
          res.status(406).send({
            text: "Email already exist.",
            color: "warning",
            icon: "mdi-account-alert-outline"
          });
        } else {
          res.status(500).send({
            text: "Internal Server Error! Please try again.",
            color: "error",
            icon: "mdi-alert-decagram-outline",
            time: 6000
          });
        }
      } else if (user) {
        mailer(req, res).catch(console.error);
        return res.status(200).send({
          text: "Registration Successful.",
          color: "success",
          icon: "mdi-account-check-outline",
          time: 0,
          addition: "Please check your mailbox to activate your account.",
          addAction: true,
          actiontype: "resend",
          email: user.email
        });
        // return res.status(200).send({ token: user })
      }
    });
  });
  
router.put("/submitFeedback",
(req, res, next) => {
  setAdmin(req, res, next);
},
 (req, res) => {
    Admin.findOneAndUpdate(
      {},
      { $push: { feedbacks: req.body } },
      { new: true },
      (err, admin) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(admin.feedbacks);
      }
    );
  });
  
  module.exports = router;
  