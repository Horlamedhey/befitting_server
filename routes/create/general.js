const { Router } = require("express");
const multer = require("multer");
const cloudinary = require("cloudinary");
const cloudinaryStorage = require("multer-storage-cloudinary");
const MainCategory = require("../../models/maincategory");
require("dotenv").config();
// const stripe = require('stripe')(process.env.stripeSecret);
cloudinary.config({
  cloud_name: process.env.cloudinaryName,
  api_key: process.env.cloudinaryKey,
  api_secret: process.env.cloudinarySecret
});
const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  folder: "blog",
  filename: function(req, file, cb) {
    cb(null, file.originalname.split(".")[0]);
  }
});
const parser = multer({ storage: storage, fileSize: 204800 });
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");

router.post(
  "/uploadImage",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  parser.single("image"),
  (req, res) => {
    if (req.file) {
      res.send(req.file.secure_url);
    } else {
      res.sendStatus(500);
    }
  }
);

router.put(
  "/addMainCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.findOneAndUpdate(
      {},
      { $push: { categories: req.body } },
      { new: true },
      (err, admin) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(admin.categories);
      }
    );
  }
);

router.put(
  "/addSubCategory",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.findOneAndUpdate(
      {},
      { $push: { moduleCategories: req.body } },
      { new: true },
      (err, admin) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(admin.moduleCategories);
      }
    );
  }
);

router.put(
  "/addTopic",
  (req, res, next) => {
    setAdmin(req, res, next);
  },
  (req, res) => {
    Admin.findOneAndUpdate(
      {},
      { $push: { skills: req.body } },
      { new: true },
      (err, admin) => {
        if (err) {
          res.sendStatus(500);
        }
        res.send(admin.skills);
      }
    );
  }
);

module.exports = router;


// router.put(
//   "/addPackages",
//   (req, res, next) => {
//     setAdmin(req, res, next);
//   }, async (req, res) => {
//     await stripe.coupons.create({
//       duration: 'once',
//       id: req.body.nickname.toLowerCase().split(' ').join('-'),
//       percent_off: 100,
//     });
//     await stripe.plans.create({
//       product: 'prod_FH0k4zizhVN4mn',
//       id: req.body.nickname.toLowerCase().split(' ').join('-') + '-trial',
//       nickname: `${req.body.nickname} (plus 7-days trial)`,
//       currency: 'usd',
//       interval: req.body.frequency.toLowerCase(),
//       interval_count: req.body.frequencyInterval,
//       amount: req.body.amount * 100,
//       trial_period_days: 7
//     });
//     const plan = await stripe.plans.create({
//       product: 'prod_FH0k4zizhVN4mn',
//       id: req.body.nickname.toLowerCase().split(' ').join('-'),
//       nickname: req.body.nickname,
//       currency: 'usd',
//       interval: req.body.frequency.toLowerCase(),
//       interval_count: req.body.frequencyInterval,
//       amount: req.body.amount * 100,
//     });
//     res.send(plan);
//   }
// );
