const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Post = require("../../models/blog/post");

router.post(
    "/addPost",
    (req, res, next) => {
      setAdmin(req, res, next);
    },
    (req, res) => {
      new Post(req.body).save()
        .then(post => {
          res.send(post);
        }
      ).catch(err => {
        res.sendStatus(500);
      });
    }
  );
  
  module.exports = router;
  