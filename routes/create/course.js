const { Router } = require("express");
const router = Router();
const setAdmin = require("../../middlewares/setAdmin");
const Course = require("../../models/course/course");

router.put(
    "/addCourse",
    (req, res, next) => {
      setAdmin(req, res, next);
    },
    (req, res) => {
      Admin.findOneAndUpdate(
        {},
        { $push: { courses: req.body } },
        { new: true },
        (err, admin) => {
          if (err) {
            res.sendStatus(500);
          }
          res.send(admin.courses)
        }
      );
    }
  );
  
  module.exports = router;
  