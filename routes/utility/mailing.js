const {
    Router
  } = require("express");
  const jsonwebtoken = require("jsonwebtoken");
  const mailer = require("../addons/mailer");
  const contactMailer = require("../addons/contactMailer");
  require("dotenv").config();
  // const jwt = require('express-jwt')
  const User = require("../models/user/user");
  const router = Router();
  const setUser = require("../middlewares/setUser");
  // jwt({ secret: process.env.secret }),
  
  router.post("/sendMail", (req, res) => {
    mailer(req, res).catch(console.error);
  });
  
  router.post("/contactMail", (req, res) => {
    contactMailer(req, res).catch(console.error);
  });
  
  module.exports = router;
  