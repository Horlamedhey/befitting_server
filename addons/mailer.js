const imagesDir = require("path").join(__dirname, "/files");
const nodemailer = require("nodemailer");
require("dotenv").config();
const User = require("../models/user/user");
const mailBody = require("./email");

async function mailer(req, res) {
  const modUser = await User.findOneAndUpdate(
    { email: req.body.email.toLowerCase() },
    { activationCode: req.body.activationCode },
    { new: true });
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "103496c4a2b092",
      pass: "e5e58868f47b1a"
    }
  });
  // setup email data with unicode symbols
  const mailOptions = {
    from: {
      name: 'Befittinglife Academy',
      address: 'support@befittinglife.com'
    }, // sender address
    // sender: "Befittinglife Academy",
    to: modUser.email, // list of receivers
    subject: "Hello ✔", // Subject line
    text: `Hey there!😃
Your account has been successfully created, we're glad to have you here.
Everybody in BefittingLife Academy, says "Hello" to you, we hope that you enjoy your stay with us.
Click the button below to activate your account.
<a href="${process.env.location}/sign?activationCode=${
      modUser.activationCode
    }">Confirm Email</a>sign
Note: This link will expire in 2 hours.`, // plain
    // text body
    html: mailBody(modUser.activationCode), // html body
    attachments: [
      {
        fileName: "49501551847444625.jpg",
        path: `${imagesDir}/49501551847444625.jpg`,
        cid: "image1"
      },
      {
        fileName: "87731551847320733.png",
        path: `${imagesDir}/87731551847320733.png`,
        cid: "image2"
      },
      {
        fileName: "92241523451206218.png",
        path: `${imagesDir}/92241523451206218.png`,
        cid: "image3"
      },
      {
        fileName: "facebook-logo-black.png",
        path: `${imagesDir}/facebook-logo-black.png`,
        cid: "image4"
      },
      {
        fileName: "google-plus-logo-black.png",
        path: `${imagesDir}/google-plus-logo-black.png`,
        cid: "image5"
      },
      {
        fileName: "linkedin-logo-black.png",
        path: `${imagesDir}/linkedin-logo-black.png`,
        cid: "image6"
      }
    ]
  };

  // send mail with defined transport object
  await transporter.sendMail(mailOptions, (err, response) => {
    if (err) {
      res.status(500);
      console.log(err);
    } else if (response) {
      console.log(response);
      res.sendStatus(200);
    }
  });
}
module.exports = mailer;
// 127.0.0.1       localhost
// 167.86.80.49    vmi245552.contaboserver.net     vmi245552
//
// # The following lines are desirable for IPv6 capable hosts
//     ::1     localhost ip6-localhost ip6-loopback
// ff02::1 ip6-allnodes
// ff02::2 ip6-allrouters
// 167.86.80.49 vmi245552.contaboserver.net vmi245552
//
// vmi245552.contaboserver.net



// erver {
//
//   root /var/www/befittinglife.com/html;
//   index index.html index.htm index.nginx-debian.html;
//
//   server_name befittinglife.com www.befittinglife.com;
//
//   location / {
//   #try_files $uri $uri/ =404;
//   proxy_pass http://localhost:3000;
//       proxy_http_version 1.1;
//   proxy_set_header Upgrade $http_upgrade;
//   proxy_set_header Connection 'upgrade';
//   proxy_set_header Host $host;
//   proxy_cache_bypass $http_upgrade;
// }
//
//   location /api/ {
//   #try_files $uri $uri/ =404;
//   proxy_pass http://localhost:4000/;
//       proxy_http_version 1.1;
//   proxy_set_header Upgrade $http_upgrade;
//   proxy_set_header Connection 'upgrade';
//   proxy_set_header Host $host;
//   proxy_cache_bypass $http_upgrade;
// }
//
//   listen [::]:443 ssl ipv6only=on; # managed by Certbot
//   listen 443 ssl; # managed by Certbot
//   ssl_certificate /etc/letsencrypt/live/befittinglife.com/fullchain.pem; # managed by Certbot
//   ssl_certificate_key /etc/letsencrypt/live/befittinglife.com/privkey.pem; # managed by Certbot
//   include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
//   ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
//
//
// }
// server {
//   if ($host = www.befittinglife.com) {
//     return 301 https://$host$request_uri;
//         } # managed by Certbot
//
//
//   if ($host = befittinglife.com) {
//     return 301 https://$host$request_uri;
//         } # managed by Certbot
//
//
//   listen 80;
//   listen [::]:80;
//
//   server_name befittinglife.com www.befittinglife.com;
//   return 404; # managed by Certbot
//
//
//
//
// }
//
