const nodemailer = require("nodemailer");
require("dotenv").config();

async function mailer(req, res) {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: "142.philmorehost.net",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: "support@befittinglife.com",
            pass: process.env.mailPass
        }
    });

    // setup email data with unicode symbols
    const mailOptions = {
        from: req.body.email, // sender address
        sender: req.body.email,
        to: "support@befittinglife.com", // list of receivers
        subject: req.body.subject, // Subject line
        text: req.body.message, // plain
    };

    // send mail with defined transport object
    await transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
            res.status(500);
        } else if (response) {
            res.status(200).send(response);
        }
    });
}
module.exports = mailer;
